#test
import functools
from sqlalchemy import create_engine
import pandas as pd

params={'host': 'localhost',
        'user': 'masteruser',
        'password': 'pass',
        'port': 5432,
        'database': 'test'}

class DBConnection:

    def __init__(self, user, host, port, database, password):
        self.conn_str = 'postgresql+psycopg2://{}:{}@{}:{}/{}'.format(user, password, host, port, database)

        self._dwh_engine = create_engine(self.conn_str)

    def connect(self):
        return self._dwh_engine.connect()

    @functools.lru_cache(maxsize=64)
    def read_sql(self, sql):
        df = pd.read_sql(sql, con=self._dwh_engine)
        return df

    def insert(self, data_frame, table):
        conn = self.connect()
        try:
            conn.execute("truncate table test_table")
            data_frame.to_sql(con=self._dwh_engine, name=table, if_exists='append', index=False)
        except:
            data_frame.to_sql(con=self._dwh_engine, name=table, if_exists='append', index=False)

        print('Values insterted sucessfully!')

db = DBConnection(**params)


