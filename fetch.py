import pandas as pd
import time
import db_connection as db

def read_convert_to_csv():
    df = pd.read_csv("http://www.tennet.org/english/operational_management/export_data.aspx?exporttype=bidpriceladder&format=csv&datefrom=2020-01-01&dateto=2020-01-02&submit=1", na_values = ['no info', '.'])
    df['processed_time'] = time.strftime("%H:%M:%S")
    df.columns = map(str.lower, df.columns)
    df = df.drop(df.index[0])
    df.to_csv('data.csv')

def insert_data_db():
    df = pd.read_csv('data.csv')
    db.db.insert(data_frame=df, table='test_table')

def create_pivot_table():
    df = db.db.read_sql('select * from test_table')
    pivot = pd.pivot(df, index='date', columns='pte', values=['total_plus'])
    print(pivot)


read_convert_to_csv()
insert_data_db()
create_pivot_table()
