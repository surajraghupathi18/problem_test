# README #

This is Devon problem test

### What is this repository for? ###

* Quick summary - Devon Assignment
* Version - 1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###


* Summary of set up

    create virtual environment
    -> virtualenv --python=python3.6 devon
    -> source ~/devon/bin/activate
    
* Dependencies
    -> pip install requiremnts.txt
* Database configuration
    -> Need create a database and change the parameters in db_connection.py
    params={'host': 'localhost',
            'user': 'masteruser',
            'password': 'pass',
            'port': 5432,
            'database': 'test'}
* How to run tests

    python fetch.py -> for task 1,2,3
    python curd_operations.py for task 4
* Deployment instructions
mar 